<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css" integrity="sha512-SnH5WK+bZxgPHs44uWIX+LLJAJ9/2PkPKZ5QiAj6Ta86w+fsb2TkcmfRyVX3pBnMFcV7oQPJkl9QevSCWr3W6A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,100..700;1,100..700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./style.css" />
    <title>CVthèque</title>
</head>

<body>
    <div class="d-flex align-items-center flex-column p-3 mb-3" id="container">
        <a href="cvtheque.php" class="text-center"><img src="img/logo-main.webp" alt="" class="w-50">
        </a>
        <a href="update.php" class="text-dark"><i class="fa-solid fa-user-plus fs-1"></i></a>
    </div>
    <div class="input-group border rounded w-25 mx-auto mb-3">
        <span class="input-group-text bg-success-subtle" id="basic-addon1"><i class="fa-solid fa-magnifying-glass"></i></span>
        <input type="search" id="searchInput" class="border border-0 form-control" placeholder="Search">
    </div>
    <div class="d-flex justify-content-around w-75 mx-auto">
        <form action="select.php" method="POST" class="d-flex justify-content-evenly w-75">
            <select class="form-select form-select-md w-50" aria-label="Large select example" name="select">
                <option selected>Mode de tri</option>
                <option value="1">Tri par nom de A à Z</option>
                <option value="2">Tri par nom de Z à A</option>
                <option value="3">Tri par ville de A à Z</option>
                <option value="4">Tri par ville de Z à A</option>
                <option value="5">Tri par profil recherché de A à Z</option>
                <option value="6">Tri par profil recherché Z à A</option>
                <option value="7">Tri par âge en ordre croissant</option>
                <option value="8">Tri par âge en ordre décroissant</option>
            </select>
            <button type="submit" class="btn btn-primary fw-bold shadow">Appliquer</button>
        </form>
    </div>
    <div class="container d-flex flex-wrap justify-content-around row-gap-5 py-5">
        <?php
        if (($handle = fopen("hrdata.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                if ($data[0] !== "Id") {
                    $csvData[] = $data;
                }
            }
            fclose($handle);
        }
        foreach ($csvData as $data) { ?>
            <div class="flip-card shadow">
                <div class="flip-card-inner ">
                    <div class="flip-card-front d-flex  flex-wrap flex-column">
                        <div class="p-2 mb-3">
                            <img src="img/logo-cards.webp" id="img-profile" class="w-50">
                        </div>
                        <!-- Afficher nom prénom -->
                        <h5><span><?php print $data[2]; ?></span>&nbsp;<span><?php print strtoupper($data[1]); ?></span></h5>
                        <!-- Afficher métier -->
                        <p class="fw-bold"><?php print $data[12]; ?></p>
                        <!-- Calculer et afficher l'âge -->
                        <?php $dateFormat = str_replace("/", "-", $data[4]);
                        $date = date_create($dateFormat);
                        $date->format('d-m-y');
                        $dateJour = date_create("now");
                        $dateJour->format('d-m-y');
                        $calcul = date_diff($date, $dateJour);
                        ?><p><?php print $calcul->format('%y ans');
                                ?></p>
                        <!-- Afficher la ville -->
                        <?php if ($data[8] !== "NULL" && $data[8] !== "") {
                        ?>
                            <p class="fw-bold"><?php print ucfirst(strtolower($data[8])); ?></p>
                        <?php } else {
                        ?> <p class="mb-5"></p> <?php
                                            } ?>
                        <!-- Afficher les compétences -->
                        <div class="p-2">

                            <?php for ($i = 13; $i < 23; $i++) {
                                if ($data[$i] !== "NULL" && $data[$i] !== "") {
                            ?>
                                    <span class="badge bg-danger-subtle
                                                fw-medium text-body-secondary p-2 mb-1"><i class="fa-solid fa-tag"></i> <?php print ucfirst($data[$i]); ?></span>
                            <?php }
                            } ?>
                        </div>
                    </div>
                    <div class="flip-card-back d-flex  flex-wrap flex-column justify-content-center">
                        <h5><span><?php print $data[2]; ?></span>&nbsp;<span><?php print strtoupper($data[1]); ?></span></h5>
                        <!-- Afficher adresse -->
                        <?php if ($data[5] !== "NULL" && $data[5] !== "") {
                        ?>
                            <p><?php print $data[5]; ?></p>
                        <?php } ?>
                        <!-- Afficher code postal et ville -->
                        <div>

                            <?php if ($data[7] !== "NULL" && $data[7] !== "") {
                            ?>
                                <span><?php print $data[7]; ?></span>
                            <?php } ?>
                            <?php if ($data[8] !== "NULL" && $data[8] !== "") {
                            ?>
                                <span><?php print ucfirst(strtolower($data[8])); ?></span>
                            <?php } ?>
                        </div>

                        <!-- Afficher numéros de téléphone et mail -->
                        <?php if ($data[9] !== "NULL" && $data[9] !== "") {
                        ?>
                            <span><?php print $data[9]; ?></span>
                        <?php } ?>
                        <?php if ($data[10] !== "NULL" && $data[10] !== "") {
                        ?>
                            <span><?php print $data[10]; ?></span>
                        <?php } ?>
                        <?php if ($data[11] !== "NULL" && $data[11] !== "") {
                        ?>
                            <span><?php print $data[11]; ?></span>
                        <?php } ?>

                        <div class="m-3">
                            <!-- Créer un lien cliquable pour envoyer un mail -->
                            <?php if ($data[11] !== "NULL") {
                            ?>
                                <a class="pe-2" href="mailto:<?php print $data[11]; ?>"><i class="fa-regular fa-envelope"></i></a>
                            <?php }

                            // Créer un lien cliquable pour télécharger les CV
                            $pdfname = './cvs/' . $data[0] . ".pdf";
                            $docxname = './cvs/' . $data[0] . ".docx";

                            // Créer une icone de téléchargement qui change selon le format du CV -->

                            if (file_exists($pdfname)) { ?>
                                <a href="<?php print $pdfname ?>" target="_blank"><i class="fa-regular fa-file-pdf"></i></a> <?php
                                                                                                                            } elseif (file_exists(($docxname))) {
                                                                                                                                ?>
                                <a href="<?php print $docxname ?>" target="_blank"><i class="fa-regular fa-file-word"></i></a>
                            <?php }
                            ?>
                        </div>
                        <div>

                            <a href="update.php?id=<?php print $data[0] ?>"><button type="button" class="btn btn-success col-4 m-1">Modifier</button></a>
                            <a href="delete.php?id=<?php print $data[0] ?>"><button type="button" class="btn btn-danger col-4 m-1">Supprimer</button></a>
                        </div>

                    </div>
                </div>
            </div>
        <?php
        }
        if (isset($_GET["add"])) { ?>
            <script>
                alert("Profil bien ajouté.")
            </script> <?php
                    }
                    if (isset($_GET["delete"])) { ?>
            <script>
                alert("Profil supprimé.")
            </script> <?php
                    }
                    if (isset($_GET["updated"])) { ?>
            <script>
                alert("Profil modifié avec succès.")
            </script> <?php
                    }
                        ?>
    </div>
    <script src="./search.js"></script>
</body>

</html>