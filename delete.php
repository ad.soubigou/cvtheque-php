<?php
if (isset($_GET['id'])) {
    $id = $_GET['id'];

    // Création d'un tableau pour récupérer les données du csv mis à jour
    $csvData = array();

    // Parcours du fichier csv
    if (($handle = fopen("hrdata.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
            // Ajout des données dans le tableau, on exclut la ligne à supprimer
            if ($data[0] != $id) {
                $csvData[] = $data;
            }
        }
    }
    fclose($handle);

    // Ouverture du fichier en mode écriture et en le vidant de son contenu ("w")
    $handle = fopen("hrdata.csv", 'w');
    // Parcours du nouveau tableau pour réécriture des données dans le fichier csv
    foreach ($csvData as $row) {
        fwrite($handle, implode(";", $row) . "\n");
    }
    fclose($handle);

    // Suppression du fichier cv correspondant à la carte
    $pdf = "./cvs/" . $id . ".pdf";
    $docx = "./cvs/" . $id . ".docx";

    if (isset($pdf)) {
        unlink($pdf);
    } else {
        exit;
    }
    if (isset($docx)) {
        unlink($docx);
    } else {
        exit;
    }

    // Redirection vers la page principale
    header("Location: cvtheque.php?delete=1");
    exit();
} else {
    print "Erreur.";
}
