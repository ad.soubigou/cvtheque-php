<?php

if ($_POST) {
    $selected = filter_input(INPUT_POST, 'select');
}

$csv = file("hrdata.csv");
$csv = array_splice($csv, 1);

switch ($selected) {
    case '1':
        if (($handle = fopen("hrdata.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                if ($data[0] != "Id")
                    $name[] = $data[1];
            }
        }
        array_multisort($name, SORT_ASC, $csv);
        break;
    case '2':
        if (($handle = fopen("hrdata.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                if ($data[0] != "Id")
                    $name[] = $data[1];
            }
        }
        array_multisort($name, SORT_DESC, $csv);
        break;
    case '3':
        if (($handle = fopen("hrdata.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                if ($data[0] != "Id")
                    $city[] = $data[8];
            }
        }
        array_multisort($city, SORT_ASC, $csv);
        break;
    case '4':
        if (($handle = fopen("hrdata.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                if ($data[0] != "Id")
                    $city[] = $data[8];
            }
        }
        array_multisort($city, SORT_DESC, $csv);
        break;
    case '5':
        if (($handle = fopen("hrdata.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                if ($data[0] != "Id")
                    $profile[] = $data[12];
            }
        }
        array_multisort($profile, SORT_ASC, $csv);
        break;
    case '6':
        if (($handle = fopen("hrdata.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                if ($data[0] != "Id")
                    $profile[] = $data[12];
            }
        }
        array_multisort($profile, SORT_DESC, SORT_NUMERIC, $csv);
        break;
    case '7':
        if (($handle = fopen("hrdata.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                if ($data[0] != "Id")
                    $age[] = $data[3];
            }
        }
        array_multisort($age, SORT_ASC, $csv);
        break;
    case '8':
        if (($handle = fopen("hrdata.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                if ($data[0] != "Id")
                    $age[] = $data[3];
            }
        }
        array_multisort($age, SORT_DESC, SORT_NUMERIC, $csv);
        break;
}

print_r($csv);
