"use strict";

const inputSkill = document.getElementById("input-skills");
const form = document.getElementById("form");
const deleteBtn = document.querySelectorAll(".delete-btn");
const taghidden = document.querySelectorAll(".hiddenTag");

function doubleTag(content) {
  // On sélectionne les div contenant les tags via leur classe
  const double = document.querySelectorAll(".skill-tag");
  for (let i = 0; i < double.length; i++) {
    // On compare si le contenu entré par l'utilisateur dans l'input est le même qu'un des contenus textuels des div
    if (
      double[i].firstChild.textContent.trim().toLowerCase() ==
      content.toLowerCase()
    ) {
      return true;
    }
  }
  return false;
}

// Ajout d'un événément sur la touche "Entrée"
inputSkill.addEventListener("keypress", function (e) {
  if (e.key === "Enter") {
    // On empêche la soumission du formulaire
    e.preventDefault();
    // Initialisation du compteur
    let tagCount = 0;
    // On parcours les inputs, si la value n'est pas vide, on incrémente le compteur
    taghidden.forEach((element) => {
      if (element.value !== "") {
        tagCount++;
      }
    });
    // On récupère la saisie utilisateur
    let content = inputSkill.value.trim();

    // On parcours tous les inputs
    for (let i = 0; i < taghidden.length; i++) {
      // Si la valeur saisie n'est pas vide, on crée un tag
      if (content !== "") {
        if (doubleTag(content) === false) {
          if (taghidden[i].value == "") {
            // la valeur de l'input devient la saisie utilisateur
            taghidden[i].value = content;
            // la div parente, contenant l'input est affichée à l'écran
            taghidden[i].parentElement.style.display = "inline-block";
            // l'élément texte contenu dans la div devient la saisie utilisateur
            taghidden[i].parentElement.firstChild.textContent = content;
            break;
          }
        } else {
          alert("compétence existante");
          break;
        }
        // Si la valeur de l'input est vide...
      }
    }
    // Le champ de l'input utilisateur est vidé
    inputSkill.value = "";
    // Le compteur est incrémenté
    tagCount++;
    // Création d'une alerte lorsque 10 tags sont créés
    if (tagCount == 10) {
      alert("Nombre de compétences maximal atteint");
      inputSkill.disabled = true;
    }
  }
});

// Parcours de tous les boutons de suppression des tags
deleteBtn.forEach(function (button) {
  // Ajout d'un événement sur les boutons
  button.addEventListener("click", function (e) {
    e.preventDefault();
    // Initialisation du compteur
    let tagCount = 0;
    // On parcours les inputs, si la value n'est pas vide, on incrémente le compteur
    taghidden.forEach((element) => {
      if (element.value !== "") {
        tagCount++;
      }
    });
    // Ciblage de la div contenant le bouton cliqué
    const parentDiv = this.closest(".skill-tag");
    // Ciblage de l'input contenu dans la div contenant aussi le bouton
    const input = parentDiv.querySelector("input");
    // Valeur de l'input devient vide
    input.value = "";
    // L'élément texte contenu dans cette div devient également vide
    parentDiv.firstChild.textContent = "";
    // L'affichage de la div disparaît
    parentDiv.style.display = "none";
    // On décrémente le compteur
    tagCount--;
    if (tagCount < 10) {
      inputSkill.disabled = false;
    }
  });
});

form.addEventListener("submit", function (e) {
  // Initialisation du compteur
  let tagCount = 0;
  // On parcours les inputs, si la value n'est pas vide, on incrémente le compteur
  taghidden.forEach((element) => {
    if (element.value !== "") {
      tagCount++;
    }
  });
  // Création une alerte si moins de 5 compétences sont entrées par l'utilisateur
  if (tagCount < 5) {
    e.preventDefault();
    alert("Veuillez entrer au moins 5 compétences.");
  }
});
