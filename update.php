<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css" integrity="sha512-SnH5WK+bZxgPHs44uWIX+LLJAJ9/2PkPKZ5QiAj6Ta86w+fsb2TkcmfRyVX3pBnMFcV7oQPJkl9QevSCWr3W6A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,100..700;1,100..700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./style.css" />
    <title>CVthèque - Form</title>
</head>

<body>
    <?php if (isset($_GET['id'])) {
        $id = $_GET["id"];
        if (($handle = fopen("hrdata.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                if ($data[0] == $id) {
                    for ($i = 0; $i < 27; $i++) {
                        if ($data[$i] === "NULL") {
                            $data[$i] = "";
                        }
                    }
                    $name = $data[1];
                    $firstname = $data[2];
                    $birthdate = $data[4];
                    $address = $data[5];
                    $address_add = $data[6];
                    $code = $data[7];
                    $city = $data[8];
                    $cellphone = $data[9];
                    $phone = $data[10];
                    $email = $data[11];
                    $profile = $data[12];
                    // Récupération de tous les skills dans un tableau (à partir de la colonne 13 du csv, on prend les 10 suivantes)
                    $skills = array_slice($data, 13, 10);
                    $site = $data[23];
                    $linkedin = $data[24];
                    $viadeo = $data[25];
                    $facebook = $data[26];
                }
            }
            fclose($handle);
        }

        if (str_contains($birthdate, "/")) {
            // On scinde la chaine de caractere en 3 éléments
            $dateFormat = explode("/", $birthdate);
            // On formate la date pour que le format corresponde à celui de l'input type date
            $formattedDate = $dateFormat[2] . "-" . $dateFormat[1] . "-" . $dateFormat[0];
            $birthdate = $formattedDate;
        }
    }
    ?>
    <div class="container p-5 col-xl-6">
        <a href="cvtheque.php">
            <button type="button" class="btn btn-primary mb-3"><i class="fa-solid fa-house"></i></button>
        </a>
        <form action="updateData.php<?php isset($id) ? print "?id=" . $id : print "" ?>" method="POST" enctype="multipart/form-data" id="form">
            <div class="mb-3 d-flex justify-content-between">
                <input type="text" name="name" class="form-control" style="width: 48%" placeholder="Nom" value="<?php isset($name) ? print $name : print "" ?>" required>
                <input type="text" name="firstname" class="form-control" style="width: 48%" placeholder="Prénom" value="<?php isset($firstname) ? print $firstname : print "" ?>" required>
            </div>
            <div class="mb-3">
                <label for="birthdate" class="form-label ps-1">Date de naissance :</label>
                <input type="date" name="birthdate" class="form-control" id="birthdate" value="<?php isset($birthdate) ? print $birthdate : print "" ?>" required>
            </div>
            <div class="mb-3">
                <textarea name="adress" class="w-100 border rounded p-2" rows="3" placeholder="Adresse"><?php isset($address) ? print $address : print "" ?></textarea>
                <textarea name="adress-add" class="w-100 border rounded p-2" rows="3" placeholder="Complément d'adresse"><?php isset($address_add) ? print $address_add : print "" ?></textarea>
            </div>
            <div class="mb-3 d-flex justify-content-between">
                <input type="text" name="code" class="form-control" style="width: 48%" placeholder="Code postal" value="<?php isset($code) ? print $code : print "" ?>">
                <input type="text" name="city" class="form-control" style="width: 48%" placeholder="Ville" value="<?php isset($city) ? print $city : print "" ?>">
            </div>
            <div class="mb-3">
                <input type="email" name="email" class="form-control" id="email" placeholder="Courriel" value="<?php isset($email) ? print $email : print "" ?>" required>
            </div>
            <div class="mb-3 d-flex justify-content-between">
                <input type="tel" name="phone" class="form-control" style="width: 48%" placeholder="Téléphone fixe" value="<?php isset($phone) ? print $phone : print "" ?>">
                <input type="tel" name="cellphone" class="form-control" style="width: 48%" placeholder="Téléphone portable" value="<?php isset($cellphone) ? print $cellphone : print "" ?>" required>
            </div>
            <div class="mb-3">
                <input type="text" name="profile" class="form-control" id="profile" placeholder="Profil" value="<?php isset($profile) ? print $profile : print "" ?>" required>
            </div>
            <div class="mb-3 d-flex flex-wrap w-100">
                <input type="text" id="input-skills" class="form-control" placeholder="Compétences">
                <div id="tags">
                    <!-- Parcours du tableau des skills pour création des tags -->
                    <?php
                    if (isset($skills)) {
                        foreach ($skills as $key => $skill) {
                            if ($skill !== "") { ?>
                                <div class="skill-tag">
                                    <?php isset($skill) ? print $skill : print "" ?>
                                    <button class="delete-btn" name="skills<?php print $key + 1 ?>"><i class="fa-solid fa-xmark"></i></button>
                                    <input type="hidden" class="hiddenTag" name="skills<?php print $key + 1 ?>" value="<?php isset($skill) ? print $skill : print "" ?>">
                                </div> <?php } else { ?>
                                <div class="skill-tag" style="display: none">
                                    <button class="delete-btn" name="skills<?php print $key + 1 ?>"><i class="fa-solid fa-xmark"></i></button>
                                    <input type="hidden" class="hiddenTag" name="skills<?php print $key + 1 ?>" value="">
                                </div>
                            <?php   }
                                }
                            } else {
                                for ($i = 1; $i <= 11; $i++) { ?>
                            <div class="skill-tag" style="display: none">
                                <button class="delete-btn" name="skills<?php print $i ?>"><i class="fa-solid fa-xmark"></i></button>
                                <input type="hidden" class="hiddenTag" name="skills<?php print $i ?>" value="">
                            </div>
                    <?php  }
                            } ?>
                </div>
            </div>
            <div class="mb-3">
                <input type="url" name="website" class="form-control" id="website" placeholder="Site internet" value="<?php isset($site) ? print $site : print "" ?>">
            </div>
            <div class="mb-3">
                <input type="url" name="linkedin" class="form-control" id="linkedin" placeholder="Lien vers le profil Linkedin" value="<?php isset($linkedin) ? print $linkedin : print "" ?>">
            </div>
            <div class="mb-3">
                <input type="url" name="viadeo" class="form-control" id="viadeo" placeholder="Lien vers le profil Viadeo" value="<?php isset($viadeo) ? print $viadeo : print "" ?>">
            </div>
            <div class="mb-3">
                <input type="url" name="facebook" class="form-control" id="facebook" placeholder="Lien vers le profil Facebook" value="<?php isset($facebook) ? print $facebook : print "" ?>">
            </div>
            <div class="mb-3">
                <label for="cv" class="form-label ps-1">CV à transmettre</label>
                <input type="file" name="cv" class="form-control" id="cv">
            </div>
            <button type="submit" class="btn btn-success" id="form-button">Envoyer</button>
        </form>
    </div>
    <script src="update.js"></script>
</body>