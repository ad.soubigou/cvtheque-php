<?php
// Cas de la modification d'une card
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_GET['id'])) {
    $id = $_GET["id"];
    // Récupération et filtrage des données du formulaire
    $name = htmlspecialchars($_POST["name"], ENT_SUBSTITUTE | ENT_HTML5);
    $firstname = htmlspecialchars($_POST["firstname"], ENT_SUBSTITUTE | ENT_HTML5);
    $birthdate = $_POST["birthdate"];
    $address = htmlspecialchars($_POST["adress"], ENT_SUBSTITUTE | ENT_HTML5);
    $address_add = htmlspecialchars($_POST["adress-add"], ENT_SUBSTITUTE | ENT_HTML5);
    $code = htmlspecialchars($_POST["code"]);
    $city = htmlspecialchars($_POST["city"], ENT_SUBSTITUTE | ENT_HTML5);
    $email = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
    $phone = filter_var($_POST["phone"], FILTER_SANITIZE_NUMBER_INT);
    $cellphone = filter_var($_POST["cellphone"], FILTER_SANITIZE_NUMBER_INT);
    $profile = htmlspecialchars($_POST["profile"], ENT_SUBSTITUTE | ENT_HTML5);
    $skills = array();
    for ($i = 1; $i <= 10; $i++) {
        $skills[$i] = htmlspecialchars($_POST["skills$i"], ENT_SUBSTITUTE | ENT_HTML5);
    }
    $website = filter_var($_POST["website"], FILTER_SANITIZE_URL);
    $linkedin = filter_var($_POST["linkedin"], FILTER_SANITIZE_URL);
    $viadeo = filter_var($_POST["viadeo"], FILTER_SANITIZE_URL);
    $facebook = filter_var($_POST["facebook"], FILTER_SANITIZE_URL);

    // Calcul de l'âge en fonction de la date de naissance 
    $dateFormat = str_replace("/", "-", $birthdate);
    $date = date_create($dateFormat);
    $date->format('d-m-y');
    $dateJour = date_create("now");
    $dateJour->format('d-m-y');
    $calcul = date_diff($date, $dateJour);
    $age = $calcul->format('%y');

    // "file" = Lit le fichier et renvoie le résultat dans un tableau
    $csv = file("hrdata.csv");

    // Ouverture du fichier csv en mode écriture, en se plaçant au début du fichier et en réduisant la taille du fichier à 0 ("w")
    $fp = fopen("hrdata.csv", "w");

    // Parcours du tableau créé correspondant au csv. Chaque élément du tableau correspond à une ligne du csv.
    foreach ($csv as $row) {
        // On scinde chaque élément du tableau en éléments indexés
        $data = explode(";", $row);
        // On attribue à chaque élément sa valeur
        if ($data[0] == $id) {
            $data[1] = $name;
            $data[2] = $firstname;
            $data[3] = $age;
            $data[4] = $birthdate;
            $data[5] = $address;
            $data[6] = $address_add;
            $data[7] = $code;
            $data[8] = $city;
            $data[11] = $email;
            $data[10] = $phone;
            $data[9] = $cellphone;
            $data[12] = $profile;
            for ($i = 13; $i < 23; $i++) {
                $data[$i] = $skills[$i - 12];
            }
            $data[23] = $website;
            $data[24] = $linkedin;
            $data[25] = $viadeo;
            $data[26] = $facebook;
            fwrite($fp, implode(";", $data) . "\n");
        } else {
            fwrite($fp, implode(";", $data));
        }
    }
    fclose($fp);

    // Récupération du cv
    // Choix du dossier où sera enregistré le fichier
    $target_dir = "./cvs/";
    // Obtention du nom du fichier uploadé par l'utilisateur
    $target_file = $target_dir . basename($_FILES["cv"]["name"]);
    // Récupération de l'extension du fichier
    $doctype = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    // Renommage du fichier avec le nouvel ID créé
    $fileName = $target_dir . $id . "." . $doctype;
    $filepdf = $target_dir . $id . ".pdf";
    $filedocx = $target_dir . $id . ".docx";
    $uploadOk = 1;
    // On vérifie si le fichier existe, si oui, on le supprime
    if (file_exists($filepdf)) {
        unlink($filepdf);
    } else if (file_exists($filedocx)) {
        unlink($filedocx);
    }
    // On limite la taille des fichiers que l'on peut uploader
    if ($_FILES["cv"]["size"] > 500000) {
        echo "Veuillez sélectionner un fichier de 500ko ou moins.";
        $uploadOk = 0;
    }
    // On limite le type de fichier
    if ($doctype != "pdf" && $doctype != "docx") {
        echo "Veuillez sélectionner un fichier pdf ou docx.";
        $uploadOk = 0;
    }
    // On vérifie qu'il n'y a pas d'erreur
    if ($uploadOk == 0) {
        echo "Erreur, le fichier n'a pas été uploadé.";
        // Si pas d'erreur, on uploade le fichier
    } else {
        if (move_uploaded_file($_FILES["cv"]["tmp_name"], $fileName)) {
            echo "Le fichier " . htmlspecialchars(basename($_FILES["cv"]["name"])) . " a bien été uploadé.";
        } else {
            echo "Erreur lors de l'upload";
        }
    }
    header("Location: cvtheque.php?updated=1");
}
// Cas de la création d'une card
else if ($_POST) { {
        $name = htmlspecialchars($_POST["name"], ENT_SUBSTITUTE | ENT_HTML5);
        $firstname = htmlspecialchars($_POST["firstname"], ENT_SUBSTITUTE | ENT_HTML5);
        $birthdate = $_POST["birthdate"];
        $adress = htmlspecialchars($_POST["adress"], ENT_SUBSTITUTE | ENT_HTML5);
        $adress_add = htmlspecialchars($_POST["adress-add"], ENT_SUBSTITUTE | ENT_HTML5);
        $code = htmlspecialchars($_POST["code"]);
        $city = htmlspecialchars($_POST["city"], ENT_SUBSTITUTE | ENT_HTML5);
        $email = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
        $phone = filter_var($_POST["phone"], FILTER_SANITIZE_NUMBER_INT);
        $cellphone = filter_var($_POST["cellphone"], FILTER_SANITIZE_NUMBER_INT);
        $profile = htmlspecialchars($_POST["profile"], ENT_SUBSTITUTE | ENT_HTML5);
        $skills = array();
        for ($i = 1; $i <= 10; $i++) {
            $skills[$i] = htmlspecialchars($_POST["skills$i"], ENT_SUBSTITUTE | ENT_HTML5);
        }
        $website = filter_var($_POST["website"], FILTER_SANITIZE_URL);
        $linkedin = filter_var($_POST["linkedin"], FILTER_SANITIZE_URL);
        $viadeo = filter_var($_POST["viadeo"], FILTER_SANITIZE_URL);
        $facebook = filter_var($_POST["facebook"], FILTER_SANITIZE_URL);
    };

    // Calcul de l'idMax pour déterminer l'id de la ligne à ajouter dans le csv
    $idMax = 0;
    if (($handle = fopen("hrdata.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
            if ($data[0] != "Id") {
                $id = $data[0];
                if ($id > $idMax) {
                    $idMax = $id;
                }
            }
        }
        fclose($handle);
    }
    $idMax++;

    // Calcul de l'âge en fonction de la date de naissance pour ajout dans le tableau
    $dateFormat = str_replace("/", "-", $birthdate);
    $date = date_create($dateFormat);
    $date->format('d-m-y');
    $dateJour = date_create("now");
    $dateJour->format('d-m-y');
    $calcul = date_diff($date, $dateJour);
    $age = $calcul->format('%y');

    // Création d'un tableau et récupération des données
    $tabData = array(
        'id' => $idMax,
        'name' => $name,
        'firstname' => $firstname,
        'age' => $age,
        'birthdate' => $birthdate,
        'adress' => $adress,
        'adress-add' => $adress_add,
        'code' => $code,
        'city' => $city,
        'cellphone' => $cellphone,
        'phone' => $phone,
        'email' => $email,
        'profile' => $profile,
        'skills1' => $skills[1],
        'skills2' => $skills[2],
        'skills3' => $skills[3],
        'skills4' => $skills[4],
        'skills5' => $skills[5],
        'skills6' => $skills[6],
        'skills7' => $skills[7],
        'skills8' => $skills[8],
        'skills9' => $skills[9],
        'skills10' => $skills[10],
        'website' => $website,
        'linkedin' => $linkedin,
        'viadeo' => $viadeo,
        'facebook' => $facebook
    );

    // Remplacement des champs vides par "NULL"
    foreach ($tabData as $key => $value) {
        if ($value === "") {
            $tabData[$key] = "NULL";
        }
    }

    // Transformation du tableau en une chaîne de caractères
    $formData = implode(";", $tabData) . "\n";

    // Récupération du cv
    if (isset($_FILES) && !empty($_FILES["cv"]["name"])) {
        // Choix du dossier où sera enregistré le fichier
        $target_dir = "./cvs/";
        // Obtention du nom du fichier uploadé par l'utilisateur
        $target_file = $target_dir . basename($_FILES["cv"]["name"]);
        // Récupération de l'extension du fichier
        $doctype = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        // Renommage du fichier avec le nouvel ID créé
        $fileName = $target_dir . $idMax . "." . $doctype;
        $uploadOk = 1;
        // On vérifie si le fichier n'existe pas déjà dans le dossier
        if (file_exists($fileName)) {
            echo "Le fichier existe déjà.";
            $uploadOk = 0;
        }
        // On limite la taille des fichiers que l'on peut uploader
        if ($_FILES["cv"]["size"] > 500000) {
            echo "Veuillez sélectionner un fichier de 500ko ou moins.";
            $uploadOk = 0;
        }
        // On limite le type de fichier
        if ($doctype != "pdf" && $doctype != "docx") { ?>
            <p>Veuillez sélectionner un fichier pdf ou docx.</p>
        <?php
            $uploadOk = 0;
        }

        // On vérifie qu'il n'y a pas d'erreur
        if ($uploadOk == 0) { ?>
            <p>Le fichier n'a pas été uploadé.</p>
            <a href="./update.php">Retour</a>
<?php }
        // Si pas d'erreur, on uploade le fichier
        else {
            if (move_uploaded_file($_FILES["cv"]["tmp_name"], $fileName)) {
                echo "Le fichier " . htmlspecialchars(basename($_FILES["cv"]["name"])) . " a bien été uploadé.";
            } else {
                echo "Erreur lors de l'upload";
            }
        }
    } else {
        $uploadOk = 1;
    }


    // Ecriture dans le fichier CSV
    if (is_writable("hrdata.csv") && ($uploadOk !== 0)) {
        // Cas où le fichier n'est pas accessible
        if (!$fp = fopen("hrdata.csv", 'a')) {
            exit;
        }
        // Message d'erreur si on ne peut pas écrire dans le fichier
        if (fwrite($fp, $formData) === FALSE) {
            echo "Erreur lors de la validation du formulaire";
            exit;
        }
        // Renvoi vers la page d'accueil à la soumission du formulaire
        header("Location: cvtheque.php?add=1");
        fclose($fp);
    }
}
